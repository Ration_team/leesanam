package loveyou.travler.ration.leesanam.util;


import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * HonestTicketForDriver
 * Class: MSharedPre
 * Created by EunChan on 2017. 12. 15..
 * <p>
 * Description:
 */
public class MSharedPre {

    private SharedPreferences mShared;

    public static final String TAG_SHARE_NAME = "leesanamshare";

    public static final String TAG_SHARE_NOTI = "appcheck";
    public static final String TAG_HOTEL_CARD = "TAG_HOTEL_CARD";
    public static final String TAG_HOTEL_MONEY = "TAG_HOTEL_MONEY";
    public static final String TAG_BUS_CARD = "TAG_BUS_CARD";
    public static final String TAG_BUS_MONEY = "TAG_BUS_MONEY";
    public static final String TAG_LIFE_CARD = "TAG_LIFE_CARD";
    public static final String TAG_LIFE_MONEY = "TAG_LIFE_MONEY";
    public static final String TAG_PERSON_CARD = "TAG_PERSON_CARD";
    public static final String TAG_PERSON_MONEY = "TAG_PERSON_MONEY";

    public static final String currentIndex = "currentIndex";

    private Context mContext;

    public MSharedPre(Context mActivity) {
        this.mContext = mActivity;

        mShared = mActivity.getSharedPreferences(TAG_SHARE_NAME,MODE_PRIVATE);
    }

    public boolean mWriteappcheck(boolean _value){
        try {
            SharedPreferences.Editor editor = mShared.edit();
            editor.putBoolean(TAG_SHARE_NOTI, _value);
            editor.commit();
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public boolean mReadappCheck(){
        boolean _Value = mShared.getBoolean(TAG_SHARE_NOTI,false);

        return _Value;
    }

    public void mWrite_money_byname(int money,String name){
        try {
            SharedPreferences.Editor editor = mShared.edit();
            editor.putInt(name, money);
            editor.commit();
        }catch (Exception e){
            return;
        }
    }
    public int mRead_money_byname(String name){
        int _Value = mShared.getInt(name,-1);

        return _Value;
    }


    public void mWrite_idx_byname(int idx){
        try {
            SharedPreferences.Editor editor = mShared.edit();
            editor.putInt(currentIndex,idx);
            editor.commit();
        }catch (Exception e){
            return;
        }
    }
    public int mRead_idx_byname(){
        int _Value = mShared.getInt(currentIndex,1);

        return _Value;
    }




}

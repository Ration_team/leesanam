package loveyou.travler.ration.leesanam.wallet;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.Format;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Wallet;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.MDEBUG;
import loveyou.travler.ration.leesanam.util.MSharedPre;
import loveyou.travler.ration.leesanam.util.db.MDBInfo;
import loveyou.travler.ration.leesanam.util.db.MDBQuery;
import loveyou.travler.ration.leesanam.util.dialog.InputMoney_Dialog;
import loveyou.travler.ration.leesanam.util.dialog.InputWallet_Dialog;

public class WalletDetailActivity extends AppCompatActivity {

    @BindView(R.id.tv_wallet_name)
    TextView tvWalletName;
    @BindView(R.id.tv_wallet_money)
    TextView tvWalletMoney;
    @BindView(R.id.tv_wallet_card)
    TextView tvWalletCard;
    @BindView(R.id.list_wallet)
    ListView listWallet;
    @BindView(R.id.fab_wallet_write)
    FloatingActionButton fabWalletWrite;
    String card_pre ="카드 :     ";
    String money_pre ="현금 :     ";

    M_Wallet wallet = new M_Wallet();

    ArrayList<M_Wallet> list_item;

    int card_Type;
    int money_Type;

    private String TYPE;
    Context mCon = WalletDetailActivity.this;
    MSharedPre sharedPre;
    String key_type_money = " ";
    String key_type_card = " ";
    WalletAdapter adapter;
    MDBQuery mdbQuery = APP.mdbQuery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_detail);
        ButterKnife.bind(this);
        TYPE = getIntent().getStringExtra(INTENT_TAG.type);

        sharedPre  = new MSharedPre(mCon);
        setinitdata();

        setUISetting();


        fabWalletWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputWallet_Dialog dlg = new InputWallet_Dialog(mCon) {
                    @Override
                    public void getWallet_bydialog(M_Wallet wallet) {

                        MDEBUG.debug("CallBack ! ");
                        wallet.type = TYPE;
                        mdbQuery.MDB_insert_wallet(wallet);
                        list_item.clear();
                        list_item.addAll(mdbQuery.MDB_getdata_wallet(TYPE));
                        setmoneyvalues();
                        adapter.notifyDataSetChanged();
                        MDEBUG.debug("change item length : " +list_item.size());

                    }
                };
                dlg.setCancelable(true);
                dlg.show();
            }
        });


        tvWalletCard.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                InputMoney_Dialog dlg = new InputMoney_Dialog(mCon) {
                    @Override
                    public void getMoney_bydialog(int money) {
                        sharedPre.mWrite_money_byname(money,key_type_card);
                        setmoneyvalues();
                    }
                };
                dlg.setCancelable(true);
                dlg.show();

                return true;
            }
        });
        tvWalletMoney.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                InputMoney_Dialog dlg = new InputMoney_Dialog(mCon) {
                    @Override
                    public void getMoney_bydialog(int money) {
                        sharedPre.mWrite_money_byname(money,key_type_money);
                        setmoneyvalues();

                    }
                };
                dlg.setCancelable(true);
                dlg.show();

                return true;
            }
        });
        listWallet.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                final int pos = position;
                AlertDialog.Builder dlg = new AlertDialog.Builder(mCon);

                dlg.setTitle("삭제할까요(하트)");
                dlg.setPositiveButton("네(하트)", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mdbQuery.mDeleteDB(MDBInfo.MTBL_WALLET,list_item.get(pos).idx);
                        list_item.remove(pos);
                        adapter.notifyDataSetChanged();
                        setmoneyvalues();
                    }
                });
                dlg.setCancelable(true);
                dlg.show();
                return true;
            }
        });
    }

    void setinitdata(){
        list_item = mdbQuery.MDB_getdata_wallet(TYPE);
        adapter = new WalletAdapter(list_item,mCon);
        listWallet.setAdapter(adapter);
        MDEBUG.debug("init item length : " +list_item.size());

    }

    void setUISetting(){

        String name = "";
        switch (TYPE){
            case "A": // hotell
                key_type_money = MSharedPre.TAG_HOTEL_MONEY;
                key_type_card = MSharedPre.TAG_HOTEL_CARD;
                name = "호텔";
                break;
            case "B": // bus
                key_type_money = MSharedPre.TAG_BUS_MONEY;
                key_type_card = MSharedPre.TAG_BUS_CARD;
                name = "교통";
                break;
            case "C": // food
                key_type_money = MSharedPre.TAG_LIFE_MONEY;
                key_type_card = MSharedPre.TAG_LIFE_CARD;
                name = "생활비";
                break;
            case "D": // personal
                key_type_money = MSharedPre.TAG_PERSON_MONEY;
                key_type_card = MSharedPre.TAG_PERSON_CARD;
                name = "개인";
                break;


        }
        tvWalletName.setText(name);


        setmoneyvalues();





    }
    public void setmoneyvalues(){
        card_Type = sharedPre.mRead_money_byname(key_type_card);
        money_Type = sharedPre.mRead_money_byname(key_type_money);

        MDEBUG.debug("Card : " + card_Type);
        MDEBUG.debug("Money : " + money_Type);
        setMoney(card_Type , money_Type);
    }


    public void setMoney(int card,int money){

        int tempcard = 0,tempmoney = 0;

        for (M_Wallet wal : list_item){
            if (wal.moneytype.equals(INTENT_TAG.card)){
                tempcard += Integer.parseInt(wal.money);
            }else if (wal.moneytype.equals(INTENT_TAG.money)){
                tempmoney += Integer.parseInt(wal.money);
            }
        }

        MDEBUG.debug("temp card : " + tempcard);
        MDEBUG.debug("temp money : " + tempmoney);
        card -= tempcard;
        money -= tempmoney;

        tvWalletCard.setText(card_pre + String.format("%,d",card));
        tvWalletMoney.setText(money_pre + String.format("%,d",money));
        if (card == -1){
            tvWalletCard.setText(card_pre + "꾸욱 눌러주세요");
        }
        if (money == -1){
            tvWalletMoney.setText(money_pre + "꾸욱 눌러주세요");
        }
    }

}

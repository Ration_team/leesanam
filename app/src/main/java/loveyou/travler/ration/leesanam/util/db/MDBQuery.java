package loveyou.travler.ration.leesanam.util.db;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import loveyou.travler.ration.leesanam.model.M_Guitar;
import loveyou.travler.ration.leesanam.model.M_Hotel;
import loveyou.travler.ration.leesanam.model.M_Schesule;
import loveyou.travler.ration.leesanam.model.M_Wallet;
import loveyou.travler.ration.leesanam.tag.db_tag.guitar_TAG;
import loveyou.travler.ration.leesanam.tag.db_tag.hotel_TAG;
import loveyou.travler.ration.leesanam.tag.db_tag.schesule_TAG;
import loveyou.travler.ration.leesanam.tag.db_tag.wallet_TAG;
import loveyou.travler.ration.leesanam.util.MDEBUG;

public class MDBQuery {
	
	/**
	 * SQLite Helper
	 */
	private MDBHelper helperSQLite;
	
	/**
	 * Database
	 */
	private SQLiteDatabase sqlDB;
	
	/**
	 * @param _context

	 */
	public MDBQuery( Context _context ) {
		// 생성
		helperSQLite = new MDBHelper(_context,MDBInfo.MDB_NAME, null, MDBInfo.MDB_VERSION);

		
		// 실 제로 DB 생성 시점
		sqlDB = helperSQLite.getReadableDatabase();
	}

	public M_Guitar MDB_getdata_guitar(String type){
		String qr = "SELECT * FROM " + MDBInfo.MTBL_GUITAR +
				" WHERE type = '" + type +"';";

		Cursor cr = sqlDB.rawQuery(qr,null);

		MDEBUG.debug("cr size = " + cr.getCount());
		MDEBUG.debug("qr = " + qr );

		M_Guitar guitar = new M_Guitar();

		while(cr.moveToNext()){


			guitar.lang = cr.getString(cr.getColumnIndex(guitar_TAG.lang));
			guitar.money_type = cr.getString(cr.getColumnIndex(guitar_TAG.money_type));
			guitar.security = cr.getString(cr.getColumnIndex(guitar_TAG.security));
			guitar.money_type = cr.getString(cr.getColumnIndex(guitar_TAG.money_type));
			guitar.time = cr.getString(cr.getColumnIndex(guitar_TAG.time));
			guitar.visa = cr.getString(cr.getColumnIndex(guitar_TAG.visa));
			guitar.weather = cr.getString(cr.getColumnIndex(guitar_TAG.weather));
			guitar.memo = cr.getString(cr.getColumnIndex(guitar_TAG.memo));

		}
		return guitar;
	}

	public void MDB_update_guitar(String memo,String type){
		String qr = "UPDATE " + MDBInfo.MTBL_GUITAR +
				" SET " + guitar_TAG.memo + " = '" + memo + "'" +
				" WHERE type = '" +type + "';";
		sqlDB.execSQL(qr);

	}
	public void MDB_update_memo(String memo,int idx){
		String qr = "UPDATE " + MDBInfo.MTBL_GUITAR +
				" SET " + guitar_TAG.memo + " = '" + memo +"'" +
				" WHERE idx = " +idx + ";";
		sqlDB.execSQL(qr);

	}
	public void MDB_update_hotel(String memo,String idx){
		String qr = "UPDATE " + MDBInfo.MTBL_HOTEL +
				" SET " + hotel_TAG.memo + " = '" + memo +"'" +
				" WHERE idx = " +idx + ";";
		sqlDB.execSQL(qr);

	}
	public ArrayList<M_Hotel> MDB_getdata_hotel(){
		String qr = "SELECT * FROM " + MDBInfo.MTBL_HOTEL +
				";";

		Cursor cr = sqlDB.rawQuery(qr,null);

		MDEBUG.debug("cr size = " + cr.getCount());
		MDEBUG.debug("qr = " + qr );

		ArrayList<M_Hotel> hotels = new ArrayList<>();

		while(cr.moveToNext()){

			M_Hotel hotel = new M_Hotel();
			hotel.idx = cr.getInt(cr.getColumnIndex(hotel_TAG.idx)) + "";
			hotel.date = cr.getString(cr.getColumnIndex(hotel_TAG.date));

			hotel.name = cr.getString(cr.getColumnIndex(hotel_TAG.name));
			hotel.location= cr.getString(cr.getColumnIndex(hotel_TAG.location));
			hotel.mapslocation = cr.getString(cr.getColumnIndex(hotel_TAG.mapslocation));
			hotel.checkin = cr.getString(cr.getColumnIndex(hotel_TAG.checkin));
			hotel.breakfirst = cr.getString(cr.getColumnIndex(hotel_TAG.breakfirst));
			hotel.price = cr.getString(cr.getColumnIndex(hotel_TAG.price));
			hotel.type = cr.getString(cr.getColumnIndex(hotel_TAG.type));
			hotel.address = cr.getString(cr.getColumnIndex(hotel_TAG.address));
			hotel.memo = cr.getString(cr.getColumnIndex(hotel_TAG.memo));

			hotels.add(hotel);
		}
		return hotels;

	}
	public M_Hotel MDB_getdata_hotel(String idx){
		String qr = "SELECT * FROM " + MDBInfo.MTBL_HOTEL +
				" WHERE idx = "+ idx +";";

		Cursor cr = sqlDB.rawQuery(qr,null);

		MDEBUG.debug("cr size = " + cr.getCount());
		MDEBUG.debug("qr = " + qr );

		M_Hotel hotel = new M_Hotel();
		while(cr.moveToNext()){


			hotel.idx = cr.getInt(cr.getColumnIndex(hotel_TAG.idx)) + "";
			hotel.date = cr.getString(cr.getColumnIndex(hotel_TAG.date));
			hotel.name = cr.getString(cr.getColumnIndex(hotel_TAG.name));
			hotel.location= cr.getString(cr.getColumnIndex(hotel_TAG.location));
			hotel.mapslocation = cr.getString(cr.getColumnIndex(hotel_TAG.mapslocation));
			hotel.checkin = cr.getString(cr.getColumnIndex(hotel_TAG.checkin));
			hotel.breakfirst = cr.getString(cr.getColumnIndex(hotel_TAG.breakfirst));
			hotel.price = cr.getString(cr.getColumnIndex(hotel_TAG.price));
			hotel.type = cr.getString(cr.getColumnIndex(hotel_TAG.type));
			hotel.address = cr.getString(cr.getColumnIndex(hotel_TAG.address));
			hotel.memo = cr.getString(cr.getColumnIndex(hotel_TAG.memo));


		}
		return hotel;

	}

	public void MDB_insert_wallet(M_Wallet data){


		String qr = "INSERT INTO " + MDBInfo.MTBL_WALLET + " ("
				+ wallet_TAG.date + ","
				+ wallet_TAG.moneytype + ","
				+ wallet_TAG.money + ","
				+ wallet_TAG.title + ","
				+ wallet_TAG.type + ") VALUES (";
		String values =
				"'" + data.date + "'," +
				"'" + data.moneytype + "'," +
				"'" + data.money + "'," +
				"'" + data.title+ "'," +
				"'" + data.type + "');";

		MDEBUG.debug(qr + values);
		sqlDB.execSQL(qr + values);
	}
	public void MDB_update_wallet(M_Wallet data){


		String qr = "UPDATE " + MDBInfo.MTBL_WALLET + " SET "
				+ wallet_TAG.date + " = '" + data.date + "' , "
				+ wallet_TAG.money + " = '" + data.money +"' , "
				+ wallet_TAG.moneytype + " = '" + data.moneytype +"' , "
				+ wallet_TAG.title + " = '" + data.title +"' , "
				+ wallet_TAG.type + " = '" + data.type
				+ "' WHERE idx = " + data.idx + ";"
				;


		sqlDB.execSQL(qr);
	}



	public ArrayList<M_Wallet> MDB_getdata_wallet(String w_type){
		String qr = "SELECT * FROM " + MDBInfo.MTBL_WALLET +
				";";

		Cursor cr = sqlDB.rawQuery(qr,null);

		MDEBUG.debug("cr size = " + cr.getCount());
		MDEBUG.debug("qr = " + qr );

		ArrayList<M_Wallet> wallets = new ArrayList<>();

		while(cr.moveToNext()){
			if (w_type.equals(cr.getString(cr.getColumnIndex(wallet_TAG.type)))) {
				M_Wallet wallet = new M_Wallet();
				wallet.idx = cr.getInt(cr.getColumnIndex(wallet_TAG.idx));
				wallet.date = cr.getString(cr.getColumnIndex(wallet_TAG.date));
				wallet.money = cr.getString(cr.getColumnIndex(wallet_TAG.money));
				wallet.moneytype = cr.getString(cr.getColumnIndex(wallet_TAG.moneytype));
				wallet.title = cr.getString(cr.getColumnIndex(wallet_TAG.title));
				wallet.type = cr.getString(cr.getColumnIndex(wallet_TAG.type));
				wallets.add(wallet);
			}
		}
		return wallets;

	}
	public boolean mDeleteDB(String name,int idx){
		String delete = "DELETE FROM " + name + " WHERE idx = " + idx + ";";


		sqlDB.execSQL(delete);

		return true;
	}

	public ArrayList<M_Schesule> MDB_getdata_schesule(){
		String qr = "SELECT * FROM " + MDBInfo.MTBL_SCHESULE +
				";";

		Cursor cr = sqlDB.rawQuery(qr,null);

		MDEBUG.debug("cr size = " + cr.getCount());
		MDEBUG.debug("qr = " + qr );
		ArrayList<M_Schesule> schesules = new ArrayList<>();
		while(cr.moveToNext()){
			M_Schesule schesule = new M_Schesule();

			schesule.idx = cr.getInt(cr.getColumnIndex(schesule_TAG.idx));
			schesule.date = cr.getString(cr.getColumnIndex(schesule_TAG.date));
			schesule.startdate = cr.getString(cr.getColumnIndex(schesule_TAG.startdate));
			schesule.title = cr.getString(cr.getColumnIndex(schesule_TAG.title));

			schesules.add(schesule);
		}
		return schesules;

	}


	/*
	 * author   :   Charny
	 * date     :   2018. 3. 30.
	 * comment  :   db close
	 *
	 */
	public void dbClose() {
		if(sqlDB != null) {
			sqlDB.close();
			sqlDB = null;
		}

		if(helperSQLite != null) {
			helperSQLite.close();
			helperSQLite = null;
		}
	}
}
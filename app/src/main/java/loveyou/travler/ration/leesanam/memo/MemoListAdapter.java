package loveyou.travler.ration.leesanam.memo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Memo;

/**
 * Created by user on 2018-03-30.
 */

public class MemoListAdapter extends BaseAdapter {

    ArrayList<M_Memo> memoList;
    Context context;

    public MemoListAdapter(ArrayList<M_Memo> memoList, Context context) {
        this.memoList = memoList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return memoList.size();
    }

    @Override
    public Object getItem(int position) {
        return memoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context mContext = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_memo, parent, false);
        }

        M_Memo memo;
        memo = memoList.get(pos);
        ViewHolder holder = new ViewHolder(convertView);

        holder.memoDate.setText(memo.date);
        holder.memoTitle.setText(memo.title);
        holder.memoContent.setText(memo.contents);

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.memo_date)
        TextView memoDate;
        @BindView(R.id.memo_title)
        TextView memoTitle;
        @BindView(R.id.memo_content)
        TextView memoContent;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

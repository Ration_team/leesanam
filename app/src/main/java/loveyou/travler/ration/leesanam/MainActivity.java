package loveyou.travler.ration.leesanam;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import loveyou.travler.ration.leesanam.fragment.Guitar_Fragment;
import loveyou.travler.ration.leesanam.fragment.Home_Fragment;
import loveyou.travler.ration.leesanam.fragment.Hotel_Fragment;
import loveyou.travler.ration.leesanam.fragment.Memo_Fragment;
import loveyou.travler.ration.leesanam.fragment.Wallet_Fragment;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.BaseActivity;

public class MainActivity extends BaseActivity {

    @BindView(R.id.main_frame)
    FrameLayout mainFrame;
    @BindView(R.id.bottomBar)
    BottomBar bottomBar;
    @BindView(R.id.time_korea)
    TextView timeKorea;
    @BindView(R.id.topbar)
    RelativeLayout topbar;

    Handler handler;
    MThread th;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);



        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_frame, Home_Fragment.newInstance()).commit();


        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(int tabId) {
                switch (tabId) {
                    case R.id.tab_home: {
                        replaceFragment(Home_Fragment.newInstance());
                        break;
                    }
                    case R.id.tab_money: {
                        replaceFragment(Wallet_Fragment.newInstance());
                        break;
                    }
                    case R.id.tab_hotel: {
                        replaceFragment(Hotel_Fragment.newInstance());
                        break;
                    }
                    case R.id.tab_etc: {
                        replaceFragment(Guitar_Fragment.newInstance());
                        break;
                    }
                    case R.id.tab_memo: {
                        replaceFragment(Memo_Fragment.newInstance());
                        break;
                    }
                }

            }
        });

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                timeKorea.setText(APP.getTime());
            }
        };



    }


    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment).commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        th.isrun = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        th = new MThread(handler);
        th.start();
    }

    class MThread extends Thread{
        public boolean isrun = true;
        Handler handler;
        public MThread(Handler handler) {
            super();
            this.handler = handler;
        }

        @Override
        public void run() {
            super.run();

            while(isrun) {
                handler.sendEmptyMessage(0);

                try {
                    this.sleep(1000 * 30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

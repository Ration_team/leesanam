package loveyou.travler.ration.leesanam.util;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by Charny on 2018-03-12.
 */

public class BaseActivity extends AppCompatActivity {
    /*
     * author : charny
     * date   : 2018-02-17
     * comment : easy move Activity
     */
    public void mGoActivity(Class target){
        Intent inte = new Intent(this,target);
        startActivity(inte);

    }

    /*
     * author : charny
     * date   : 2018-02-17
     * comment :easy Toast by String resauce id
     */
    public void mToastShow(int _text_id) {
        Toast.makeText(getBaseContext(), _text_id, Toast.LENGTH_SHORT).show();
    }
    /*
     * author : charny
     * date   : 2018-02-17
     * comment :easy Toast by String
     */
    public void mToastShow(String _text) {
        Toast.makeText(getBaseContext(), _text, Toast.LENGTH_SHORT).show();
    }
}

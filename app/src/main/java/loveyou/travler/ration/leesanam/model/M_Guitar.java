package loveyou.travler.ration.leesanam.model;

/**
 * Created by kimeunchan on 2018. 3. 30..
 */

public class M_Guitar {

    public M_Guitar(){}

    public M_Guitar(String idx, String type, String lang, String time, String visa, String money_type, String weather, String security) {
        this.idx = idx;
        this.type = type;
        this.lang = lang;
        this.time = time;
        this.visa = visa;
        this.money_type = money_type;
        this.weather = weather;
        this.security = security;
    }

    public String idx = "idx";
    public String type = "type";
    public String lang = "lang";
    public String time = "time";
    public String visa = "visa";
    public String money_type = "moneytype";
    public String weather = "weather";
    public String security = "security";
    public String memo = "memo";

}

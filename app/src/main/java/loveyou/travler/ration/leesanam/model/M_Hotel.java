package loveyou.travler.ration.leesanam.model;

/**
 * Created by kimeunchan on 2018. 3. 30..
 */

public class M_Hotel {

    public M_Hotel(){}
    public String idx = "idx";
    public String name = "name";
    public String address = "address";
    public String price = "price";
    public String memo = "memo";
    public String date = "date";
    public String location = "location";
    public String type = "type";
    public String breakfirst = "breakfirst";
    public String checkin = "checkin";
    public String mapslocation = "mapslocation";

}

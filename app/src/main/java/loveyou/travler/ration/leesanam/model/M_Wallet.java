package loveyou.travler.ration.leesanam.model;

/**
 * Created by kimeunchan on 2018. 3. 30..
 */

public class M_Wallet {

    public M_Wallet(int idx, String type, String date, String money, String moneytype, String title) {
        this.idx = idx;
        this.type = type;
        this.date = date;
        this.money = money;
        this.moneytype = moneytype;
        this.title = title;
    }
    public M_Wallet(){}

    public int idx;
    public String type = "type";
    public String date = "date";
    public String money = "money";
    public String moneytype = "moneytype";
    public String title = "title";

}

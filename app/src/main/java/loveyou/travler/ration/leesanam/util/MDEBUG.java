package loveyou.travler.ration.leesanam.util;


import android.util.Log;

/**
 * Created by Charny on 2018-03-12.
 */

public class MDEBUG {

    /*
     * author : charny
     * date   : 2018-03-12
     * comment :
     */
    private static String header = "<--debug-->>>>>";
    private static boolean mDebugmode =
            true;
    //            false;
    public static void debug(String msg){
        if (mDebugmode)
            Log.d(header,msg);
    }
}

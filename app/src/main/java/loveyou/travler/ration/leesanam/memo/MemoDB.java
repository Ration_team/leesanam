package loveyou.travler.ration.leesanam.memo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import loveyou.travler.ration.leesanam.model.M_Memo;
import loveyou.travler.ration.leesanam.tag.db_tag.memo_TAG;
import loveyou.travler.ration.leesanam.util.db.MDBHelper;
import loveyou.travler.ration.leesanam.util.db.MDBInfo;

/**
 * Created by user on 2018-03-30.
 */

public class MemoDB {

    private MDBHelper helper;
    private Context context;

    public MemoDB(Context context,MDBHelper helper) {
        this.context=context;
        this.helper=helper;
    }

    public void addMemo(M_Memo memo){
        SQLiteDatabase db=helper.getWritableDatabase();

        StringBuffer sb=new StringBuffer();
        sb.append(" INSERT INTO ").append(MDBInfo.MTBL_MEMO).append(" ( ");
        sb.append(memo_TAG.date).append(",");
        sb.append(memo_TAG.title).append(",");
        sb.append(memo_TAG.contents).append(" ) ");
        sb.append(" VALUES ( ?, ?, ? )");

        db.execSQL(sb.toString(),new Object[]{
                memo.date,
                memo.title,
                memo.contents});

        db.close();
        helper.close();
    }

    public ArrayList<M_Memo> getMemoALL(){

        StringBuffer sb=new StringBuffer();
        sb.append(" SELECT ").append(memo_TAG.idx).append(",");
        sb.append(memo_TAG.date).append(",");
        sb.append(memo_TAG.title).append(",");
        sb.append(memo_TAG.contents);
        sb.append(" FROM ").append(MDBInfo.MTBL_MEMO);

        if(helper==null){
            return null;
        }else{
            SQLiteDatabase db=helper.getReadableDatabase();

            Cursor cursor=db.rawQuery(sb.toString(),null);
            ArrayList<M_Memo> memoList=new ArrayList<>();
            M_Memo memo=null;

            while(cursor.moveToNext()){
                memo=new M_Memo();
                memo.idx=cursor.getInt(0);
                memo.date=cursor.getString(1);
                memo.title=cursor.getString(2);
                memo.contents=cursor.getString(3);

                memoList.add(memo);
            }
            db.close();
            helper.close();
            return memoList;
        }
    }

    public boolean mDeleteMemo(int idx){
        String delete = "DELETE FROM " + MDBInfo.MTBL_MEMO + " WHERE idx = " + idx + ";";

        if(helper==null){
            return false;
        }else {
            SQLiteDatabase db=helper.getReadableDatabase();
            db.execSQL(delete);
            db.close();
            helper.close();
        }

        return true;
    }
    public M_Memo getMemo(int idx){
        SQLiteDatabase db;
        StringBuffer sb=new StringBuffer();
        sb.append(" SELECT ").append(memo_TAG.idx).append(",");
        sb.append(memo_TAG.date).append(",");
        sb.append(memo_TAG.title).append(",");
        sb.append(memo_TAG.contents);
        sb.append(" FROM ").append(MDBInfo.MTBL_MEMO)
        .append(" WHERE idx = " + idx);

        if(helper==null){
            return null;
        }else{
            db=helper.getReadableDatabase();

            Cursor cursor=db.rawQuery(sb.toString(),null);
            ArrayList<M_Memo> memoList=new ArrayList<>();
            M_Memo memo=null;
            memo=new M_Memo();
            memo.idx=cursor.getInt(0);
            memo.date=cursor.getString(1);
            memo.title=cursor.getString(2);
            memo.contents=cursor.getString(3);

            db.close();
            helper.close();
            return memo;

        }

    }

}

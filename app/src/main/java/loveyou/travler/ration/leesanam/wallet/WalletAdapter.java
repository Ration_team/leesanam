package loveyou.travler.ration.leesanam.wallet;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Wallet;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;

/**
 * Created by kimeunchan on 2018. 3. 31..
 */

public class WalletAdapter extends BaseAdapter {

    public ArrayList<M_Wallet> walletList;
    Context context;

    Drawable card;
    Drawable money;

    public WalletAdapter(ArrayList<M_Wallet> walletList, Context context) {
        this.walletList = walletList;
        this.context = context;

        card = context.getResources().getDrawable(R.drawable.card_type);
        money = context.getResources().getDrawable(R.drawable.money_type);
    }

    public WalletAdapter(Context context) {
        this.context = context;

        card = context.getResources().getDrawable(R.drawable.card_type);
        money = context.getResources().getDrawable(R.drawable.money_type);
    }
    @Override
    public int getCount() {
        return walletList.size();
    }

    @Override
    public Object getItem(int position) {
        return walletList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context mContext = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_wallet, parent, false);
        }

        M_Wallet wallet;
        wallet = walletList.get(pos);
        ViewHolder holder = new ViewHolder(convertView);


        holder.walletDate.setText(wallet.date);
        holder.walletTitle.setText(wallet.title);
        holder.walletMoney.setText(String.format("%,d",wallet.money));

        if (wallet.moneytype.equals(INTENT_TAG.money)){
            holder.walletIcon.setBackground(money);
        }else{
            holder.walletIcon.setBackground(card);

        }
        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.wallet_date)
        TextView walletDate;
        @BindView(R.id.wallet_title)
        TextView walletTitle;
        @BindView(R.id.wallet_money)
        TextView walletMoney;
        @BindView(R.id.wallet_icon)
        ImageView walletIcon;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}



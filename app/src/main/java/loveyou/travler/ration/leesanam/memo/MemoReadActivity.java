package loveyou.travler.ration.leesanam.memo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Memo;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.BaseActivity;
import loveyou.travler.ration.leesanam.util.db.MDBQuery;

/**
 * Created by user on 2018-03-30.
 */

public class MemoReadActivity extends BaseActivity {

    @BindView(R.id.read_img_back)
    ImageView readImgBack;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.memo_read_title)
    TextView memoReadTitle;
    @BindView(R.id.memo_read_content)
    TextView memoReadContent;
    M_Memo memo;
    @BindView(R.id.memo_read_date_bar)
    RelativeLayout memoReadDateBar;
    @BindView(R.id.read_title_base)
    TextView readTitleBase;
    @BindView(R.id.memo_read_title_bar)
    RelativeLayout memoReadTitleBar;
    @BindView(R.id.base)
    ImageView base;
    @BindView(R.id.fab_memo_write)
    FloatingActionButton fabMemoWrite;
    @BindView(R.id.edt_memo_edit)
    EditText edtMemoEdit;
    @BindView(R.id.btn_memo_btn)
    Button btnMemoBtn;
    @BindView(R.id.rl_memo_edit)
    RelativeLayout rlMemoEdit;

    private Context mCon = MemoReadActivity.this;
    MDBQuery mdbQuery;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memo_read);
        ButterKnife.bind(this);

        //DB 호출 후 M_Memo Class 저장

//        UISetting(memo);
        mdbQuery = APP.mdbQuery;


        setIntentdata(getIntent());


        btnMemoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memoReadContent.setVisibility(View.VISIBLE);
                fabMemoWrite.setVisibility(View.VISIBLE);
                rlMemoEdit.setVisibility(View.GONE);
                memoReadContent.setText(edtMemoEdit.getText());
                mdbQuery.MDB_update_memo(edtMemoEdit.getText().toString(),memo.idx);
            }
        });
        fabMemoWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memoReadContent.setVisibility(View.GONE);
                fabMemoWrite.setVisibility(View.GONE);
                rlMemoEdit.setVisibility(View.VISIBLE);

                edtMemoEdit.setText(memoReadContent.getText());
            }
        });

    }

    private void setIntentdata(Intent data) {

        memo = new M_Memo();

        memo.idx = data.getIntExtra(INTENT_TAG.idx, -1);
        memo.title = data.getStringExtra(INTENT_TAG.title);
        memo.date = data.getStringExtra(INTENT_TAG.date);
        memo.contents = data.getStringExtra(INTENT_TAG.contents);

        UISetting(memo);
    }


    private void UISetting(M_Memo memo) {
        title.setText(memo.date);
        memoReadTitle.setText(memo.title);
        memoReadContent.setText(memo.contents);
    }

    @OnClick(R.id.read_img_back)
    public void onViewClicked() {
        finish();
    }
}

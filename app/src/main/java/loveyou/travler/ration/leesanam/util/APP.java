package loveyou.travler.ration.leesanam.util;

import android.app.Application;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.Locale;

import loveyou.travler.ration.leesanam.util.db.MDBQuery;

/**
 * Created by Charny on 2018-03-12.
 */

public class APP extends Application{


    public static MDBQuery mdbQuery;
    public static MSharedPre mSharedPre;
    @Override
    public void onCreate() {
        super.onCreate();
        mSharedPre = new MSharedPre(getApplicationContext());
        mdbQuery = new MDBQuery(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mdbQuery.dbClose();
    }

    /*
           * author : charny
           * date   : 2018-02-17
           * comment :
           */
    public static Date getCurrentDate(){
        Date date = new Date();
        Calendar cal = new GregorianCalendar(Locale.KOREA);
        date = cal.getTime();

        return date;
    }

    /*
     * author : charny
     * date   : 2018-02-17
     * comment :
     */
    public static int getCurrentYear (){

        return new GregorianCalendar(Locale.ITALY).get(Calendar.YEAR);
    }

    /*
     * author : charny
     * date   : 2018-02-17
     * comment :
     */
    public static int getCurrentMonth (){
        return new GregorianCalendar(Locale.ITALY).get(Calendar.MONTH) + 1;
    }

    /*
    * author : charny
    * date   : 2018-02-17
    * comment :
    */
    public static int getCurrentDay (){
        return new GregorianCalendar(Locale.ITALY).get(Calendar.DATE);
    }

    public static String getMD(){
        return getCurrentMonth() + "." + getCurrentDay();
    }

    public static String getTime(){
        Format formatter = new SimpleDateFormat("MM월dd일  HH시mm분");
        return "" + formatter.format(getCurrentDate());
    }
}

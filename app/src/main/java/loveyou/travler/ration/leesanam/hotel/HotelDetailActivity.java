package loveyou.travler.ration.leesanam.hotel;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Hotel;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.BaseActivity;
import loveyou.travler.ration.leesanam.util.db.MDBQuery;

public class HotelDetailActivity extends BaseActivity {

    @BindView(R.id.top_name_bar)
    RelativeLayout topNameBar;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_checkin)
    TextView tvCheckin;
    @BindView(R.id.tv_breakfirst)
    TextView tvBreakfirst;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.inform_con)
    LinearLayout informCon;
    @BindView(R.id.btn_google_connect)
    Button btnGoogleConnect;
    @BindView(R.id.tv_con_memo)
    TextView tvConMemo;
    @BindView(R.id.btn_con_edit)
    Button btnConEdit;
    @BindView(R.id.edt_con_edit)
    EditText edtConEdit;
    @BindView(R.id.rl_con_edit)
    RelativeLayout rlConEdit;

    MDBQuery mdbQuery = APP.mdbQuery;
    M_Hotel hotel;
    @BindView(R.id.tv_hotel_name)
    TextView tvHotelName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);
        ButterKnife.bind(this);
        setIntentdata(getIntent());

        btnConEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlConEdit.setVisibility(View.GONE);
                tvConMemo.setVisibility(View.VISIBLE);
                tvConMemo.setText(edtConEdit.getText());

                mdbQuery.MDB_update_hotel(edtConEdit.getText().toString(),getIntent().getStringExtra(INTENT_TAG.idx));
            }
        });
        tvConMemo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                rlConEdit.setVisibility(View.VISIBLE);
                tvConMemo.setVisibility(View.GONE);
                edtConEdit.setText(tvConMemo.getText());
                return true;
            }
        });
        btnGoogleConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hotel.mapslocation.isEmpty()){
                    Toast.makeText(HotelDetailActivity.this, "주소 정보가 없습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + hotel.mapslocation);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
    }


    private void setIntentdata(Intent data) {

        hotel = mdbQuery.MDB_getdata_hotel(data.getStringExtra(INTENT_TAG.idx));
        UISetting(hotel);
    }


    private void UISetting(M_Hotel hotel) {
        tvAddress.setText(hotel.address);
        tvHotelName.setText(hotel.location + "\n" + hotel.name + "\n" + hotel.date);
        tvBreakfirst.setText(hotel.breakfirst);
        tvConMemo.setText(hotel.memo);
        tvPrice.setText(hotel.price);
        tvType.setText(hotel.type);
        tvCheckin.setText(hotel.checkin);
    }
}

package loveyou.travler.ration.leesanam.util.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import loveyou.travler.ration.leesanam.R;

/**
 * Created by kimeunchan on 2018. 4. 1..
 */

public abstract class InputMoney_Dialog extends Dialog implements View.OnClickListener{


    EditText cdialogInputmoneyEdit;
    TextView tv;
    Button cdialogInputmoneyConfirm;

    public InputMoney_Dialog(@NonNull Context context) {
        super(context);
    }

    public abstract void getMoney_bydialog(int money);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cdialog_input_money);
        cdialogInputmoneyEdit = (EditText) findViewById(R.id.cdialog_inputmoney_edit);
        cdialogInputmoneyConfirm = (Button) findViewById(R.id.cdialog_inputmoney_confirm);
        tv = (TextView)findViewById(R.id.temp_tv_money);
        cdialogInputmoneyConfirm.setOnClickListener(this);

        cdialogInputmoneyEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.length() != 0)
                        tv.setText(String.format("%,d", Integer.parseInt(s.toString())));
                }catch (Exception e){
                    tv.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.cdialog_inputmoney_confirm:
                if (cdialogInputmoneyEdit.getText().toString() == null){
                    Toast.makeText(getContext(), "내용을 입력해주세요", Toast.LENGTH_SHORT).show();
                    return;
                }
                int money = Integer.parseInt(cdialogInputmoneyEdit.getText().toString());
                getMoney_bydialog(money);
                this.dismiss();
                break;
        }
    }
}

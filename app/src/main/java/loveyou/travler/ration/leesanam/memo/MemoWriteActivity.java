package loveyou.travler.ration.leesanam.memo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Memo;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.BaseActivity;
import loveyou.travler.ration.leesanam.util.db.MDBHelper;
import loveyou.travler.ration.leesanam.util.db.MDBInfo;

/**
 * Created by user on 2018-03-30.
 */

public class MemoWriteActivity extends BaseActivity implements TextWatcher {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.memo_write_submit)
    TextView memoWriteSubmit;
    @BindView(R.id.memo_edt_title)
    EditText memoEdtTitle;
    @BindView(R.id.memo_edt_content)
    EditText memoEdtContent;

    private static final String FINISH = "완료";
    private static final String CANCEL = "닫기";
    private static final String DOT = ".";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memo_write);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        memoEdtTitle.addTextChangedListener(this);
        DateTitle();
    }

    private void DateTitle() {
        APP app = (APP) getApplicationContext();
        String date = app.getCurrentYear() +
                DOT + app.getCurrentMonth() +
                DOT + app.getCurrentDay();
        title.setText(date);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.toString().equals("")) {
            memoWriteSubmit.setText(CANCEL);
        } else {
            memoWriteSubmit.setText(FINISH);
        }
    }

    @OnClick(R.id.memo_write_submit)
    public void onViewClicked() {
        if (memoWriteSubmit.getText().toString().equals(FINISH)){
            MDBHelper helper=new MDBHelper(this, MDBInfo.MTBL_MEMO,null,MDBInfo.MDB_VERSION);
            MemoDB memoDB=new MemoDB(this,helper);
            M_Memo memo=new M_Memo();
            memo.date=title.getText().toString();
            memo.title=memoEdtTitle.getText().toString();
            memo.contents=memoEdtContent.getText().toString();

            memoDB.addMemo(memo);
            Intent intent=getIntent();
            setResult(RESULT_OK,intent);
            finish();
        }
        else{
            finish();
        }
    }
}

package loveyou.travler.ration.leesanam.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.wallet.WalletDetailActivity;

/**
 * Created by Charny on 2018-03-28.
 */

public class Wallet_Fragment extends Fragment {

    private static Wallet_Fragment main_frag;
    @BindView(R.id.wallet_frame_1)
    ImageView walletFrame1;
    @BindView(R.id.wallet_frame_2)
    ImageView walletFrame2;
    @BindView(R.id.wallet_frame_3)
    ImageView walletFrame3;
    @BindView(R.id.wallet_frame_4)
    ImageView walletFrame4;
    Unbinder unbinder;

    public static Wallet_Fragment newInstance() {
        if (main_frag == null) {
            main_frag = new Wallet_Fragment();
        }
        return main_frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wallet_frame, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.wallet_frame_1, R.id.wallet_frame_2, R.id.wallet_frame_3, R.id.wallet_frame_4})
    public void onViewClicked(View view) {
        Intent inte = new Intent(getContext(),WalletDetailActivity.class);
        switch (view.getId()) {
            case R.id.wallet_frame_1:
                inte.putExtra(INTENT_TAG.type,INTENT_TAG.hotel);
                break;
            case R.id.wallet_frame_2:
                inte.putExtra(INTENT_TAG.type,INTENT_TAG.bus);
                break;
            case R.id.wallet_frame_3:
                inte.putExtra(INTENT_TAG.type,INTENT_TAG.life);
                break;
            case R.id.wallet_frame_4:
                inte.putExtra(INTENT_TAG.type,INTENT_TAG.person);
                break;
        }
        startActivity(inte);

    }
}

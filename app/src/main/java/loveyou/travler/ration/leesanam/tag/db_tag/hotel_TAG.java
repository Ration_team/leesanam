package loveyou.travler.ration.leesanam.tag.db_tag;

/**
 * Created by kimeunchan on 2018. 3. 30..
 */

public class hotel_TAG {
    public static final String idx = "idx";
    public static final String name = "name";
    public static final String address = "address";
    public static final String price = "price";
    public static final String memo = "memo";
    public static final String date = "date";
    public static final String location = "location";
    public static final String type = "type";
    public static final String breakfirst = "breakfirst";
    public static final String checkin = "checkin";
    public static final String mapslocation = "mapslocation";

}

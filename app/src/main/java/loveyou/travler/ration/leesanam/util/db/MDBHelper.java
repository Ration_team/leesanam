package loveyou.travler.ration.leesanam.util.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.tag.db_tag.*;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.MDEBUG;


public class MDBHelper extends SQLiteOpenHelper {
	/**
	 * Table Name
	 */
	public static final String TBL_SAVE_DATA = "travlerDB";

	private Context mContext;
	/**
	 * @param _context
	 * @param _name
	 * @param _factory
	 * @param _version
	 */
	public MDBHelper( Context _context, String _name,
					CursorFactory _factory, int _version ) {
		super( _context, _name, _factory, _version );
		mContext = _context;
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		String _Schesule_table =
				("create table if not exists " + MDBInfo.MTBL_SCHESULE +
						" ("+ schesule_TAG.idx +" INTEGER," +
						" "+ schesule_TAG.date +" TEXT," +
						""+ schesule_TAG.title +" TEXT," +
						""+ schesule_TAG.startdate +" TEXT)"
				);
		MDEBUG.debug("MDBHelper onCreate!");
		db.execSQL(_Schesule_table);
		String _Wallet_table =
				("create table if not exists " + MDBInfo.MTBL_WALLET +
						" ("+ wallet_TAG.idx +" INTEGER PRIMARY KEY AUTOINCREMENT," +
						" "+ wallet_TAG.type +" TEXT," +
						""+ wallet_TAG.date +" TEXT," +
						""+ wallet_TAG.money +" TEXT," +
						""+ wallet_TAG.moneytype +" TEXT," +
						""+ wallet_TAG.title +" TEXT)"
				);
		db.execSQL(_Wallet_table);

		String _Hotel_table =
				("create table if not exists " + MDBInfo.MTBL_HOTEL +
						" ("+ hotel_TAG.idx +" INTEGER PRIMARY KEY AUTOINCREMENT," +
						" "+ hotel_TAG.date +" TEXT," +
						" "+ hotel_TAG.name +" TEXT," +
						""+ hotel_TAG.location +" TEXT," +
						""+ hotel_TAG.mapslocation +" TEXT," +
						""+ hotel_TAG.breakfirst +" TEXT," +
						""+ hotel_TAG.type +" TEXT," +
						""+ hotel_TAG.checkin +" TEXT," +
						""+ hotel_TAG.address +" TEXT," +
						""+ hotel_TAG.price +" TEXT," +
						""+ hotel_TAG.memo +" TEXT);"
				);
		// insert  (all ) , update (etc)
		db.execSQL(_Hotel_table);

		String _Guitar_table =
				("create table if not exists " + MDBInfo.MTBL_GUITAR +
						" ("+ guitar_TAG.idx +" INTEGER PRIMARY KEY AUTOINCREMENT," +
						" "+ guitar_TAG.type +" TEXT," +
						""+ guitar_TAG.lang +" TEXT," +
						""+ guitar_TAG.time +" TEXT," +
						""+ guitar_TAG.visa +" TEXT," +
						""+ guitar_TAG.weather +" TEXT," +
						""+ guitar_TAG.money_type +" TEXT," +
						""+ guitar_TAG.security +" TEXT," +
						""+ guitar_TAG.memo +" TEXT);"
				);
		db.execSQL(_Guitar_table);

		String _Memo_table =
				("create table if not exists " + MDBInfo.MTBL_MEMO +
						" ("+ memo_TAG.idx +" INTEGER PRIMARY KEY AUTOINCREMENT," +
						" "+ memo_TAG.date +" TEXT," +
						""+ memo_TAG.title +" TEXT," +
						""+ memo_TAG.contents +" TEXT);"
				);
		db.execSQL(_Memo_table);

		setdatas(db);


	}
	

	@Override
	public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion ) {
		//DEBUG_.debug("onUpgrade oldVersion : " + oldVersion + ", newVersion : " + newVersion);
	}
	
	public void setdatas(SQLiteDatabase db){
		// TODO: 2018. 3. 30.  hotel , guitar , schesule init

		MDEBUG.debug("DB init ready");
		if (!APP.mSharedPre.mReadappCheck()){
			MDEBUG.debug("DB init start");

			initguitar(db);
			inithotel(db);
			initscheshul(db);
		}else{
			MDEBUG.debug("DB init Already init");

		}

		APP.mSharedPre.mWriteappcheck(true);

	}
	void inithotel(SQLiteDatabase db){
		String qrpre = "INSERT INTO " + MDBInfo.MTBL_HOTEL + "("
				+ hotel_TAG.date+ ","
				+ hotel_TAG.name + ","
				+ hotel_TAG.price+ ","
				+ hotel_TAG.address + ","
				+ hotel_TAG.location + ","
				+ hotel_TAG.mapslocation + ","
				+ hotel_TAG.breakfirst + ","
				+ hotel_TAG.checkin+ ","
				+ hotel_TAG.type+ ","
				+ hotel_TAG.memo + ") VALUES (";

		String arr[] = new String[12];

		arr[0] = "'4.3 ~ 4.7','프라이드 / 한인민박','342(300+도시세 1박당 약 3.5유로) // 현지 267유로 현금지불','via principe amedeo 148번지'" +
				",'로마','via principe amedeo 148','한식 6:30~7:30','13시 / 10시 *짐보관가능','여성 도미토리','*1박가격 75유로(105,000원) 선입금 / 도착 후 벨한번만 누르기'";
		arr[1] = "'4.7 ~ 4.9','소조르노 이사벨라 데 메디치','194/255,694(176+도시세 1박당 약 3유로) // 현지 현금지불','Via Fiume 17, 50123 피렌체, 이탈리아'," +
				"'피렌체','Via Fiume 17, 50123 피렌체, 이탈리아','X','14시 / 11시','게스트하우스 - 트리플룸 / 싱글침대 3개 / 개인 욕실있음','*메시지 보냄. / 예약확인서x, 1시 체크인가능 가격맞음'";
		arr[2] = "'4.9 ~ 4.11','design apartment','257,411 //지불완료','Viale Andrea Doria 8 Int. 26 Piano 8 밀라노 Lombardia 20124 // 8층','밀라노','Viale Andrea Doria, 8 Int. 26 Piano 8 밀라노, Lombardia 20124','X','11시 / ???  *유동적','집 한채','*메시지 보냄. // 건물에 컨시어지있음.'";
		arr[3] = "'4.11 ~ 4.14','아파트먼트 에센스 오브 베니스','407.5/537,089(345+도시세&청소비62.5) // 현금지불','cannaregio Calle Priuli Ai Cavalletti N°105 secondo piano','베니스','cannaregio Calle Priuli Ai Cavalletti N°105 secondo piano','X','14시 / 10시 *오후3-5에 도착예정 메시지보냄','2베드룸 아파트 / 집 한채 / 세탁기 있음','*메시지 보냄.'";
		arr[4] = "'4.14 ~ 4.17','지그재그 로빈 올드타운 아파트먼트','273(264+도시세9) // 숙박비는 카드자동결제 / 도시세만 현장지불','S. Chiurco 8, 52210 로비니, 크로아티아','로빈','Trg Maršala Tita, 52210, Rovinj, 크로아티아','X','15시 / 11시','아파트 1채',''";
		arr[5] = "'4.17 ~ 4.18','apartment t & d','30/39,540 // 4.1카드자동결제 여유롭게 42000원 ','Gladijatorska 33, 52100 풀라, 크로아티아','풀라','Gladijatorska 33, 52100 풀라, 크로아티아','X','14시 / 10시','아파트 1층','*메시지 보냄.  //예약 확인서x'";
		arr[6] = "'4.18 ~ 4.20','apartment p70','104/137,073(98+도시세6) / 현장결제','70 Palmotićeva ulica','자그레브','70 Palmotićeva ulica','X','14시 / 11시','집 한채','체크인 방법 : Ruđera Boškovića or Ulica Josipa Ruđera Boškovića 17 // 대리점으로가서 돈 지불\n영업시간 10-22\nTel: +385 1 466 9235 / Mob: +385 99 331 4480. \n집주인 385 91 181 8975\n*메시지 보냄. / 가격맞음, 예약확인서x'";
		arr[7] = "'4.20 ~ 4.22','게스트하우스','170,825 // 결제 완료','Westbahnstraße, 1070 Wien, 오스트리아','비엔나','','X','15시 / 11시','아파트 개인실',''";
		arr[8] = "'4.22 ~ 4.24','요호 호스텔','155.64(145.5+도시세 9)  // 135.59 현장지불','Paracelsusstr. 9, A 5020 Salzburg','잘츠부르크','Paracelsusstr. 9, A 5020 Salzburg','X','3시 / 11시','6인 여성전용 도미토리 / 화장실포함','*20.04유로(24.72달러) 선입금 // 3시이후 체크인 '";
		arr[9] = "'4.24 ~ 4.28','펜션 메안드르(meandr)','259.20/341,628(252+도시세 7.2) // 현지 코루나현금지불','Rybářská 1 체스키크룸로프 381 01','체스키쿠룸로프','Rybářská 1 381 01','가능','13:00/10:30','트리플룸 / 전용욕실 / 침대2개','*메시지 보냄. // 가격 맞음.'";
		arr[10] = "'4.28 ~ 4.30','U빅토리','145/191,111 (도시세3.6) // 현지지불','U Dobrenskych 271/5, 1st floor, 프라하','프라하1','U Dobrenskych 271/5, 1st floor','X','14시 / 10시','아파트 1채',''";
		arr[11] = "'4.30 ~ 5.3','올드로열포스트아파트먼트','748,267','Maltézské Náměstí 8 Prague 01 프라하 118 00 Czech Republic','프라하2','Maltézské Náměstí 8, Prague 01, 프라하, 118 00, Czech Republic','X','15시 / 11시','아파트 한채 더블1/쇼파1',''";


		String qrpost = ");";
		for (String i : arr){
			MDEBUG.debug(qrpre + i + qrpost);
			db.execSQL(qrpre + i + qrpost);
		}
	}

	public void initguitar(SQLiteDatabase db){
		String qrpre = "INSERT INTO " + MDBInfo.MTBL_GUITAR + "("
				+ guitar_TAG.lang + ","
				+ guitar_TAG.money_type + ","
				+ guitar_TAG.time + ","
				+ guitar_TAG.visa + ","
				+ guitar_TAG.weather + ","
				+ guitar_TAG.security + ","
				+ guitar_TAG.type+ ","
				+ guitar_TAG.memo + ") VALUES (";

		String italy = "'이태리어','유로','7시간늦음*DST','무비자'," +
				"'일교차심함 겉옷필수 비가 종종내림','역주변조심','"
				+ INTENT_TAG.italy+"','한국에서구입하기 30일권 3만원중반'";
		String Checo = "'체코어','코루나','7시간늦음*DST','무비자'," +
				"'일교차심함 겉옷필수 비가 종종내림','양호.','"
				+ INTENT_TAG.Checo+"','한국에서구입하기 30일권 3만원중반'";
		String Croatia = "'크로아티아어','유로->쿠나','7시간늦음*DST','무비자'," +
				"'일교차심함 겉옷필수 비가 종종내림','양호.','"
				+ INTENT_TAG.Croatia+"','한국에서구입하기 30일권 3만원중반'";
		String Ostrailia = "'독일어','유로','양호.*DST','무비자'," +
				"'일교차심함 겉옷필수 비가 종종내림','역주변조심','"
				+ INTENT_TAG.Ostrailia+"','한국에서구입하기 30일권 3만원중반'";
		String qrpost = ");";
		MDEBUG.debug("DB initial italy : " +qrpre + italy + qrpost );
		MDEBUG.debug("DB initial Checo : " +qrpre + Checo + qrpost );
		MDEBUG.debug("DB initial Croatia : " +qrpre + Croatia + qrpost );
		MDEBUG.debug("DB initial Ostrailia : " +qrpre + Ostrailia + qrpost );

		db.execSQL(qrpre + italy + qrpost);
		db.execSQL(qrpre + Checo + qrpost);
		db.execSQL(qrpre + Croatia + qrpost);
		db.execSQL(qrpre + Ostrailia + qrpost);
	}
	void initscheshul(SQLiteDatabase db){
		String qrpre = "INSERT INTO " + MDBInfo.MTBL_SCHESULE + "("
				+ schesule_TAG.date+ ","
				+ schesule_TAG.startdate + ","
				+ schesule_TAG.idx+ ","
				+ schesule_TAG.title + ") VALUES (";

		String arr[] = new String[21];

		arr[0] = "'4.3 ~ 4.7','4.3',1,'로마'";
		arr[1] = "'4.7','4.7',2,'로마 -> 피렌체'";
		arr[2] = "'4.7 ~ 4.9','4.8',3,'피렌체'";
		arr[3] = "'4.9','4.9',4,'피렌체 -> 밀라노'";
		arr[4] = "'4.9 ~ 4.11','4.10',5,'밀라노'";
		arr[5] = "'4.11','4.11',6,'밀라노 -> 베니스'";
		arr[6] = "'4.11 ~ 4.14','4.12',7,'베니스'";
		arr[7] = "'4.14','4.14',8,'베니스 -> 로빈'";
		arr[8] = "'4.14 ~ 4.17','4.15',9,'로빈'";
		arr[9] = "'4.17','4.17',10,'로빈 -> 풀라'";
		arr[10] = "'4.18','4.18',11,'풀라 -> 자그레브'";
		arr[11] = "'4.18 ~ 4.20','4.19',12,'자그레브'";
		arr[12] = "'4.20','4.20',13,'자그레브 -> 비엔나'";
		arr[13] = "'4.20 ~ 4.22','4.21',14,'비엔나'";
		arr[14] = "'4.22','4.22',15,'비엔나 -> 잘츠부르크'";
		arr[15] = "'4.22 ~ 4.24','4.23',16,'잘츠부르크'";
		arr[16] = "'4.24','4.24',17,'잘츠부르크 -> 체스키쿠롬르프'";
		arr[17] = "'4.24 ~ 4.28','4.25',18,'체스키쿠롬르프'";
		arr[18] = "'4.28','4.28',19,'체스키쿠롬르프 -> 프라하'";
		arr[19] = "'4.28 ~ 5.3','4.29',20,'프라하'";
		arr[20] = "'5.3','5.3',21,'프라하 -> 서울'";



		String qrpost = ");";
		for (String i : arr){
			MDEBUG.debug(qrpre + i + qrpost);
			db.execSQL(qrpre + i + qrpost);
		}
	}


}
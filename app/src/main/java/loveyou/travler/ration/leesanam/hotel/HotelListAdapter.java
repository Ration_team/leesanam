package loveyou.travler.ration.leesanam.hotel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Hotel;

/**
 * Created by user on 2018-03-30.
 */

public class HotelListAdapter extends BaseAdapter {

    ArrayList<M_Hotel> hotelList;
    Context context;

    public HotelListAdapter(ArrayList<M_Hotel> memoList, Context context) {
        this.hotelList = memoList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return hotelList.size();
    }

    @Override
    public Object getItem(int position) {
        return hotelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context mContext = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_hotel, parent, false);
        }

        M_Hotel hotel;
        hotel = hotelList.get(pos);
        ViewHolder holder = new ViewHolder(convertView);

        holder.hotelTitle.setText(hotel.location);
        holder.hotelDate.setText(hotel.date);

        return convertView;
    }


    class ViewHolder {
        @BindView(R.id.hotel_title)
        TextView hotelTitle;
        @BindView(R.id.hotel_date)
        TextView hotelDate;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

package loveyou.travler.ration.leesanam.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Schesule;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.MDEBUG;
import loveyou.travler.ration.leesanam.util.MSharedPre;
import loveyou.travler.ration.leesanam.util.db.MDBQuery;
import loveyou.travler.ration.leesanam.util.dialog.InputMoney_Dialog;
import loveyou.travler.ration.leesanam.util.dialog.testdla;

/**
 * Created by Charny on 2018-03-28.
 */

public class Home_Fragment extends Fragment {

    private static Home_Fragment main_frag;
    @BindView(R.id.today_date_tv)
    TextView todayDateTv;
    @BindView(R.id.today_content_tv)
    TextView todayContentTv;
    @BindView(R.id.tomorrow_date_tv)
    TextView tomorrowDateTv;
    @BindView(R.id.tomorrow_content_tv)
    TextView tomorrowContentTv;
    Unbinder unbinder;

    ArrayList<M_Schesule> list_item;
    MSharedPre mSharedPre;
    MDBQuery mdbQuery = APP.mdbQuery;
    @BindView(R.id.sc_back_img)
    ImageView scBackImg;

    public static Home_Fragment newInstance() {
        if (main_frag == null) {
            main_frag = new Home_Fragment();
        }
        return main_frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_frame, container, false);
        unbinder = ButterKnife.bind(this, view);
        mSharedPre = APP.mSharedPre;

        initData(mSharedPre.mRead_idx_byname());

        try{
            settingData();

        }catch (Exception e){
            MDEBUG.debug(e.toString());
        }

//
//        todayDateTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                testdla test = new testdla(getContext()) {
//                    @Override
//                    public void getMoney_bydialog(String day) {
//                        settingData2(day);
//                    }
//                };
//                test.show();
//            }
//        });
        return view;

    }

    public void initData(int idx) {
        list_item = mdbQuery.MDB_getdata_schesule();

        MDEBUG.debug("Home idx : " + idx);
        MDEBUG.debug("LIST size : "+list_item.size());
        if (idx  < list_item.size()) {
            MDEBUG.debug("1 if true");
            for (M_Schesule _idx : list_item){
                if (idx == _idx.idx){
                    MDEBUG.debug("2 if true");

                    todayContentTv.setText(_idx.title);
                    todayDateTv.setText(_idx.date);

                }
                else if (idx + 1 == _idx.idx){
                    tomorrowDateTv.setVisibility(View.VISIBLE);
                    tomorrowContentTv.setVisibility(View.VISIBLE);
                    tomorrowContentTv.setText(_idx.title);
                    tomorrowDateTv.setText(_idx.date);
                    return;
                }
            }
        } else if (idx  == list_item.size()) {
            tomorrowDateTv.setVisibility(View.GONE);
            tomorrowContentTv.setVisibility(View.GONE);
            todayDateTv.setText(list_item.get(20).date);
            todayContentTv.setText(list_item.get(20).title);
        }
        else{
            MDEBUG.debug("All False");

        }

    }

    public void settingData() {
        list_item = mdbQuery.MDB_getdata_schesule();

        for (M_Schesule sc : list_item) {

            if (
//                    APP.getMD()
                    "4.3"
                            .equals(sc.startdate)) {// 현재 일  == 시작일
                MDEBUG.debug("settingData");
                mSharedPre.mWrite_idx_byname(sc.idx);
                initData(mSharedPre.mRead_idx_byname());

            }
        }
    }
    public void settingData2(String date) {
        list_item = mdbQuery.MDB_getdata_schesule();
        MDEBUG.debug("date : " + date);

        for (M_Schesule sc : list_item) {

            if (
                    date
                            .equals(sc.startdate)) {// 현재 일  == 시작일
                MDEBUG.debug("settingData");
                mSharedPre.mWrite_idx_byname(sc.idx);
                initData(mSharedPre.mRead_idx_byname());

            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

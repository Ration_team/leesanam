package loveyou.travler.ration.leesanam.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.memo.MemoDB;
import loveyou.travler.ration.leesanam.memo.MemoListAdapter;
import loveyou.travler.ration.leesanam.memo.MemoReadActivity;
import loveyou.travler.ration.leesanam.memo.MemoWriteActivity;
import loveyou.travler.ration.leesanam.model.M_Memo;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.util.MDEBUG;
import loveyou.travler.ration.leesanam.util.db.MDBHelper;
import loveyou.travler.ration.leesanam.util.db.MDBInfo;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Charny on 2018-03-28.
 */

public class Memo_Fragment extends Fragment {

    private static Memo_Fragment main_frag;
    private static int REQUEST_DATA=1;

    @BindView(R.id.memo_list)
    ListView memoList;
    @BindView(R.id.memo_emptyTv)
    TextView memoEmptyTv;
    @BindView(R.id.fab_memo_write)
    FloatingActionButton fabMemoWrite;
    Unbinder unbinder;

    MemoListAdapter mMemoListAdapter;
    MDBHelper helper;
    MemoDB memoDB;
    View view;

    ArrayList<M_Memo> memoArrayList;
    public static Memo_Fragment newInstance() {
        if (main_frag == null) {
            main_frag = new Memo_Fragment();
        }
        return main_frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.memo_frame, container, false);
        unbinder = ButterKnife.bind(this, view);
        if(helper==null){
            helper=new MDBHelper(getContext(), MDBInfo.MTBL_MEMO,null,MDBInfo.MDB_VERSION);
        }
        MDEBUG.debug("helper : " + helper+"");
        memoDB=new MemoDB(getContext(),helper);
        memoArrayList=memoDB.getMemoALL();
        if(memoArrayList==null||memoArrayList.isEmpty()){
            memoList.setEmptyView(view.findViewById(R.id.memo_emptyTv));
        }else{
            mMemoListAdapter = new MemoListAdapter(memoArrayList,getContext());
            memoList.setAdapter(mMemoListAdapter);
        }
        memoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });


        memoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                M_Memo m_memo = memoArrayList.get(position);

                Intent inte = new Intent(getContext(),MemoReadActivity.class);
                inte.putExtra(INTENT_TAG.idx,m_memo.idx);
                inte.putExtra(INTENT_TAG.title,m_memo.title);
                inte.putExtra(INTENT_TAG.date,m_memo.date);
                inte.putExtra(INTENT_TAG.contents,m_memo.contents);
                startActivity(inte);
            }
        });
        memoList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                final int pos = position;
                AlertDialog.Builder dlg = new AlertDialog.Builder(getContext());

                dlg.setTitle("");
                dlg.setMessage("삭제??");
                dlg.setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO: 2018. 3. 30. delete bu idx pos
                        int idx = memoArrayList.get(pos).idx;
                        memoArrayList.remove(pos);
                        memoDB.mDeleteMemo(idx);

                        mMemoListAdapter.notifyDataSetChanged();

                    }
                });
                dlg.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });

                dlg.create().show();

                return true;
            }
        });

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab_memo_write)
    public void onViewClicked() {
        Intent intent=new Intent(getContext(), MemoWriteActivity.class);
        startActivityForResult(intent,REQUEST_DATA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_DATA){
            if(resultCode==RESULT_OK){
                if(helper!=null){
                    helper=new MDBHelper(getContext(), MDBInfo.MTBL_MEMO,null,MDBInfo.MDB_VERSION);
                }
                memoDB=new MemoDB(getContext(),helper);
                memoArrayList=memoDB.getMemoALL();
                if(memoArrayList==null||memoArrayList.isEmpty()){
                    memoList.setEmptyView(view.findViewById(R.id.memo_emptyTv));
                }else{
                    mMemoListAdapter = new MemoListAdapter(memoArrayList,getContext());
                    mMemoListAdapter.notifyDataSetChanged();
                    memoList.setAdapter(mMemoListAdapter);
                }
            }
        }
    }
}

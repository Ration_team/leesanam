package loveyou.travler.ration.leesanam.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.hotel.HotelDetailActivity;
import loveyou.travler.ration.leesanam.hotel.HotelListAdapter;
import loveyou.travler.ration.leesanam.model.M_Hotel;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.db.MDBQuery;

/**
 * Created by Charny on 2018-03-28.
 */

public class Hotel_Fragment extends Fragment {

    private static Hotel_Fragment main_frag;
    @BindView(R.id.hotel_list)
    ListView hotelList;
    Unbinder unbinder;
    private MDBQuery mdbQuery = APP.mdbQuery;
    private HotelListAdapter adapter;
    ArrayList<M_Hotel> hotel_list = new ArrayList<>();

    public static Hotel_Fragment newInstance() {
        if (main_frag == null) {
            main_frag = new Hotel_Fragment();
        }
        return main_frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hotel_frame, container, false);
        unbinder = ButterKnife.bind(this, view);

        addListfromDB();

        hotelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String idx = hotel_list.get(position).idx;

                Intent inte = new Intent(getContext(),HotelDetailActivity.class);
                inte.putExtra(INTENT_TAG.idx,idx);
                startActivity(inte);
            }
        });
        return view;
    }

    void setAdapter(){
        adapter = new HotelListAdapter(hotel_list,getContext());
        hotelList.setAdapter(adapter);
    }
    void addListfromDB(){

        hotel_list = mdbQuery.MDB_getdata_hotel();

        setAdapter();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

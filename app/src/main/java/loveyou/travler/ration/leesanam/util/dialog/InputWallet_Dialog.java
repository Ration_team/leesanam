package loveyou.travler.ration.leesanam.util.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Wallet;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.MDEBUG;

/**
 * Created by kimeunchan on 2018. 4. 1..
 */

public abstract class InputWallet_Dialog extends Dialog implements View.OnClickListener {


    @BindView(R.id.cdiloag_inputwallet_title)
    TextView cdiloagInputwalletTitle;
    @BindView(R.id.rb_inputwallet_card)
    RadioButton rbInputwalletCard;
    @BindView(R.id.rb_inputwallet_money)
    RadioButton rbInputwalletMoney;
    @BindView(R.id.rg_inputwallet_moneytype)
    RadioGroup rgInputwalletMoneytype;
    @BindView(R.id.cdialog_inputwallet_edit)
    EditText cdialogInputwalletEdit;
    @BindView(R.id.ll_inputwallet_money)
    LinearLayout llInputwalletMoney;
    @BindView(R.id.cdialog_inputwallet_titleedit)
    EditText cdialogInputwalletTitleedit;
    @BindView(R.id.ll_inputwallet_title)
    LinearLayout llInputwalletTitle;
    @BindView(R.id.base)
    View base;
    @BindView(R.id.cdialog_inputwallet_confirm)
    Button cdialogInputwalletConfirm;
    private String money_type;
    TextView tv;

    public abstract void getWallet_bydialog(M_Wallet wallet);

    public InputWallet_Dialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cdialog_inputwallet);

        rgInputwalletMoneytype = (RadioGroup) findViewById(R.id.rg_inputwallet_moneytype);
        cdialogInputwalletConfirm = (Button) findViewById(R.id.cdialog_inputwallet_confirm);
        cdialogInputwalletEdit = (EditText) findViewById(R.id.cdialog_inputwallet_edit);
        cdialogInputwalletTitleedit = (EditText) findViewById(R.id.cdialog_inputwallet_titleedit);
        rbInputwalletCard = (RadioButton) findViewById(R.id.rb_inputwallet_card);
        rgInputwalletMoneytype.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_inputwallet_card:
                        money_type = INTENT_TAG.card;
                        break;
                    case R.id.rb_inputwallet_money:
                        money_type = INTENT_TAG.money;
                        break;
                }
            }
        });
        tv= (TextView)findViewById(R.id.temp_tv_wallet);
        rbInputwalletCard.setChecked(true);
        money_type = INTENT_TAG.card;
        cdialogInputwalletConfirm.setOnClickListener(this);

        cdialogInputwalletEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.length() != 0)
                        tv.setText(String.format("%,d", Integer.parseInt(s.toString())));
                }catch (Exception e){
                    tv.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    public void onClick(View view) {
        MDEBUG.debug("Click!!");
        switch (view.getId()) {

            case R.id.cdialog_inputwallet_confirm:
                MDEBUG.debug("Click!! Confirm");

                M_Wallet wallet = new M_Wallet();

                if (cdialogInputwalletEdit.getText().toString().isEmpty() || cdialogInputwalletTitleedit.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), "내용을 입력해주세요", Toast.LENGTH_SHORT).show();
                    return;
                }
                wallet.moneytype = money_type;
                wallet.money = cdialogInputwalletEdit.getText().toString();
                wallet.date = APP.getCurrentMonth() + "월 " + APP.getCurrentDay() + "일";
                wallet.title = cdialogInputwalletTitleedit.getText().toString();
                getWallet_bydialog(wallet);
                this.dismiss();
                break;
        }
    }
}

package loveyou.travler.ration.leesanam.util.db;


/*
 * author : charny
 * date   : 2018-03-27
 * comment : 
 */
public class MDBInfo {
	/**
	 * DB File Name
	 */
	public static final String MDB_NAME			= "leesanam.sqlite";
	public static final String MTBL_SCHESULE			= "schesuletbl";
	public static final String MTBL_WALLET			= "wallettbl";
	public static final String MTBL_GUITAR			= "guitartbl";
	public static final String MTBL_HOTEL			= "hoteltbl";
	public static final String MTBL_MEMO			= "memotbl";

	/**
	 * DB Version
	 */
	public static final int MDB_VERSION				= 1;
}

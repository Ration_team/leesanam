package loveyou.travler.ration.leesanam.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.guitar.GuitarDetailActivity;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;

/**
 * Created by Charny on 2018-03-28.
 */

public class Guitar_Fragment extends Fragment {

    private static Guitar_Fragment main_frag;
    @BindView(R.id.guitar_frame_1)
    LinearLayout guitarFrame1;
    @BindView(R.id.guitar_frame_2)
    LinearLayout guitarFrame2;
    @BindView(R.id.guitar_frame_3)
    LinearLayout guitarFrame3;
    @BindView(R.id.guitar_frame_4)
    LinearLayout guitarFrame4;
    Unbinder unbinder;

    public static Guitar_Fragment newInstance() {
        if (main_frag == null) {
            main_frag = new Guitar_Fragment();
        }
        return main_frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.guitar_frame, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.guitar_frame_1, R.id.guitar_frame_2, R.id.guitar_frame_3, R.id.guitar_frame_4})
    public void onViewClicked(View view) {
        Intent inte = new Intent(getContext(),GuitarDetailActivity.class);
        switch (view.getId()) {
            case R.id.guitar_frame_1:
                inte.putExtra(INTENT_TAG.type,INTENT_TAG.italy);
                inte.putExtra(INTENT_TAG.NationName,"이탈리아");
                break;
            case R.id.guitar_frame_2:
                inte.putExtra(INTENT_TAG.type,INTENT_TAG.Croatia);
                inte.putExtra(INTENT_TAG.NationName,"크로아티아");
                break;
            case R.id.guitar_frame_3:
                inte.putExtra(INTENT_TAG.type,INTENT_TAG.Ostrailia);
                inte.putExtra(INTENT_TAG.NationName,"오스트레일리아");
                break;
            case R.id.guitar_frame_4:
                inte.putExtra(INTENT_TAG.type,INTENT_TAG.Checo);
                inte.putExtra(INTENT_TAG.NationName,"체코");
                break;
        }
        startActivity(inte);

    }

}

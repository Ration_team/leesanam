package loveyou.travler.ration.leesanam.model;

/**
 * Created by kimeunchan on 2018. 3. 30..
 */

public class M_Memo {
    public M_Memo(int idx, String title, String contents, String date) {
        this.idx = idx;
        this.title = title;
        this.contents = contents;
        this.date = date;
    }
    public M_Memo(){}
    public int idx;
    public String title;
    public String contents;
    public String date;
}

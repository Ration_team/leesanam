package loveyou.travler.ration.leesanam.guitar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import loveyou.travler.ration.leesanam.R;
import loveyou.travler.ration.leesanam.model.M_Guitar;
import loveyou.travler.ration.leesanam.tag.INTENT_TAG;
import loveyou.travler.ration.leesanam.util.APP;
import loveyou.travler.ration.leesanam.util.BaseActivity;
import loveyou.travler.ration.leesanam.util.MDEBUG;
import loveyou.travler.ration.leesanam.util.db.MDBQuery;

public class GuitarDetailActivity extends BaseActivity {

    @BindView(R.id.tv_country_name)
    TextView tvCountryName;
    @BindView(R.id.rl_guitar_topbar)
    RelativeLayout rlGuitarTopbar;
    @BindView(R.id.tv_guitar_lang)
    TextView tvGuitarLang;
    @BindView(R.id.tv_guitar_moneytype)
    TextView tvGuitarMoneytype;
    @BindView(R.id.tv_guitar_weather)
    TextView tvGuitarWeather;
    @BindView(R.id.tv_guitar_security)
    TextView tvGuitarSecurity;
    @BindView(R.id.inform_con)
    LinearLayout informCon;
    @BindView(R.id.tv_guitar_memo)
    TextView tvGuitarMemo;
    @BindView(R.id.btn_guitar_edit)
    Button btnGuitarEdit;
    @BindView(R.id.edt_guitar_edit)
    EditText edtGuitarEdit;
    @BindView(R.id.rl_guitar_edit)
    RelativeLayout rlGuitarEdit;
    MDBQuery mdbQuery;
    @BindView(R.id.tv_guitar_visa)
    TextView tvGuitarVisa;
    @BindView(R.id.tv_guitar_time)
    TextView tvGuitarTime;
    private Context mCon = GuitarDetailActivity.this;
    private M_Guitar guitar;
    private String C_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guitar_detail);
        ButterKnife.bind(this);
        mdbQuery = APP.mdbQuery;

        setinitdata(getIntent());

        tvGuitarMemo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                tvGuitarMemo.setVisibility(View.GONE);
                rlGuitarEdit.setVisibility(View.VISIBLE);
                edtGuitarEdit.setText(tvGuitarMemo.getText());
                return true;
            }
        });

        btnGuitarEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvGuitarMemo.setVisibility(View.VISIBLE);
                rlGuitarEdit.setVisibility(View.GONE);
                tvGuitarMemo.setText(edtGuitarEdit.getText());
                mdbQuery.MDB_update_guitar(edtGuitarEdit.getText().toString(),C_type);

            }
        });



    }


    private void setinitdata(Intent data) {
        C_type =data.getStringExtra(INTENT_TAG.type);

        MDEBUG.debug("contry type = " + C_type);
        guitar = mdbQuery.MDB_getdata_guitar(C_type);

        UISetting(guitar, data.getStringExtra(INTENT_TAG.NationName));

    }

    private void UISetting(M_Guitar data, String name) {

        tvCountryName.setText(name);
        tvGuitarVisa.setText(data.visa);
        tvGuitarLang.setText(data.lang);
        tvGuitarMoneytype.setText(data.money_type);
        tvGuitarSecurity.setText(data.security);
        tvGuitarTime.setText(data.time);
        tvGuitarWeather.setText(data.weather);
        tvGuitarMemo.setText(data.memo);

    }
}
